all: mindfulness_the_game.pdf

mindfulness_the_game.pdf : mindfulness_the_game.md
	pandoc -o mindfulness_the_game.pdf mindfulness_the_game.md

clean:
	rm *.pdf
