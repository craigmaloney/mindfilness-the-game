# Mindfulness, the game

**Written by**: Craig Maloney

**Version**: 0.3

**License**: [Creative Commons Attribution-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/)

**Dedicated to**: JoDee, for everything.

**Mindfulness, the game** is a solo journaling game about being mindful. It uses a series of prompts to help you be more in the moment and allow yourself to focus on being present in this moment.

## Safety tools

This game can stir up uncomfortable thoughts. If at any time you encounter a thought that you don't want to pursue then give yourself permission to just let that thought pass by. If you are unable to let it go then write "Here ends the session." and give yourself time to recover. This game is about exploration of our thoughts, not dredging up painful or discomforting thoughts.

If you are up to it you might want to discuss those thoughts with a therapist or a trusted companion at a later date. This is completely optional and is outside of the scope of this game.

## Equipment

The following equipment is completely optional but may help you to better focus:

### Prompts

* A six-sided "question" die with the words "who", "what", "where", "when", "why", and "how" on each side (available from Steve Jackson Games, Koplow, and others).
* Alternatively, a six sided die with the numbers 1-6 on each side.

### Logging (optional)

* A pad of paper with a writing implement (pen, pencil, crayon, etc.).
* Alternatively, A computer with a text editor or word processor.

### A timer (optional)

* An hourglass or electronic timer
* A timer on your phone, computer

### Agitation (optional)

* Three tokens for marking when you get agitated through this entire process
* Alternatively, you can mark "agitated" in your journal

Note that the simpler the tools the better the experience this game will offer.

## The Game

The main goal of the game is to have you think about this moment in time and focus on it. The prompts are there to help you focus on one particular aspect of this moment in time.

If you would like to do a timed session set your timer for the desired amount. Short sessions may be 5-10 minutes, and a long session may be as long as 30 minutes to an hour. When starting off with this game you will want to start with 5-10 minute sessions. The recommended time for more advanced players is 30 minutes but any amount of time will do.

Alternatively, you may play this where the game is finished when you have explored each of the prompts (who, what, where, when, why, and how).

### Choosing a prompt

Roll the die to determine which of the prompts you will focus on for the next few minutes. If you have a standard six-sided die you may use the following table for the prompt (e.g.: if you roll a 1 then use the prompt "who?". Rolling a 2 equals the prompt "what?", etc.)

| number | prompt |
|--------|--------|
| 1      | who?   |
| 2      | what?  |
| 3      | where? |
| 4      | when?  |
| 5      | why?   |
| 6      | how?   |

Take a few seconds to think about the prompt. Many times we'll already have something in our mind relating to the prompt, but if not the feel free to use the following as a guide:

* **Who?**: This can relate to other people or ourselves. Who is on our mind at the moment? Who do we want to show kindness to? Who are we in this moment?
* **What?**: This can relate to items or ideas that we have. What is in front of us? What does this moment feel like? What has our attention at this moment?
* **Where?**: This can relate to places or mental space. Where are we currently playing this game? Where is our mind wanting to go? Where do we fit in the grand scheme of the cosmos?
* **When?**: This can relate to time. When are we playing this game? When is a good time to stop playing? When is this moment in time?
* **Why?**: This can relate to purpose, motivation, or current feelings. Why are we playing this game? Why have we chosen this moment? Why do we feel the way we feel?
* **How?**: This can relate to our current state. How are we playing this game? How do we currently feel? How would we like to show up?

These are just some ideas to get you started. You can interpret these prompts however you wish.

### Write about it

Once you have given the prompt some thought the next part is to write about it. _Don't overthink this._ There's no wrong answer to these prompts. Whatever is grabbing your attention is what you should write about. Spend the next few minutes writing about what comes to mind.

Keep in mind that this game works best with honesty but is not about trying to dredge up things that are uncomfortable. If you run into uncomfortable or painful thoughts then please refer to the **Safety Tools** section above for how best to handle them.

**Remember**: this is a game about exploration of our thoughts. There's no right or wrong way to do this.

### Agitation

During the game you'll notice yourself getting agitated, especially if you've set a timer and that time hasn't elapsed yet. That's part of the mindfulness process. Just note that you experienced agitation, either by moving a token or marking "agitated" in your journal. Then try to return back to the process of writing.

### Repeat

Once you have completed your thoughts on the given prompt you may roll the die again for the next prompt. Refer to the steps in **Choosing a prompt** and **Write about it** for what to do for each prompt you roll.

If you get the same prompt this session you may re-roll the die or continue exploring the thoughts around that prompt.

If you have noted "agitation" three times then stop the timer. Move to **Finishing the game**.

### Finishing the game

You may complete the game when you have explored each prompt, marked three "agitation", or when the timer goes off. Write down any additional thoughts you've had during this session and give yourself some gratitude for giving yourself space to explore your thoughts. Acknowledge any thoughts that were difficult or uncomfortable for you as being a part of you.

When you have completed this final wrap-up the game is complete.

### Daily Practice

This game can be part of a daily practice for journaling or meditation. Obviously the writing part would be optional for meditation, but it can be a good reminder to focus on one aspect of our lives at a time and give ourselves the space to explore those thoughts as they occur.

## Tips

Here are some tips for playing this game

* **Don't Rush!**: Take your time with this exercise. There are no bonus points for finishing quickly or beating the timer. (We checked).
* **Relax and Pace Yourself**: There might be points where you feel overwhelmed or bored by this game. Pace yourself. Allow yourself to be within the moment relax into it.
* **TNSTAC**: "There's no such thing as correct". However you play this is the correct way. This isn't a competition. (We also checked).
* **You are enough**: This isn't really part of the game but it bears repeating. You, in this moment, are enough.

## Designer Notes

This game came out of realizing that I had an abundance (read: too many) question dice. I didn't realize they came in a pack of 10. I thought about how best to use these dice and came up with the idea of using them for a journaling game of some form. There are several ways I could have approached this (dungeon crawl, walk through a town, etc.) but the first one that appealed to me was the idea of using it as a meditation tool for mindfulness. There might be others down the line as time and creativity permit, but I felt this one needed my attention first. Leave it to me to pick the least fantastical version of things to start with.

I hope you enjoy this exercise. If you have any comments or would like to share your journey feel free to contact me at [https://decafbad.net/contact](https://decafbad.net/contact).
